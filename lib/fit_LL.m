function [ power_for_Fit, yfit, slope ] = fit_LL( photon_density, Pin_muW, above_or_below )
%FIT_LL Summary of this function goes here
%   Detailed explanation goes here
if ~strcmp(above_or_below, 'above') && ~strcmp(above_or_below, 'below')   
    error('string should be either "above" or "below" ');
end

if strcmp(above_or_below, 'below')   
    initial_x = [Pin_muW(1) Pin_muW(floor(length(Pin_muW)/2))];
else if strcmp(above_or_below, 'above')   
    initial_x = [Pin_muW(ceil(length(Pin_muW)/2)) Pin_muW(end)];
    end
end

titleStr = strcat('Select the range for', {' '}, above_or_below, ' threshold');
figure;
xlabel('Pump power (\mu W)')
loglog(Pin_muW, photon_density, 'b-', 'linewidth', 2); 
ylabel('Photon density (cm^{-2})')
title(titleStr)
dualcursor(initial_x)  % Turn on cursors
pause
val = dualcursor;  % get the current cursor positions
pause(0.1)
close gcf
valx = val([1,3]);
index1 = find(Pin_muW == valx(1), 1);
index2 = find(Pin_muW == valx(2), 1);

power_for_Fit = log10(Pin_muW(index1:index2)) ;
photon_for_Fit = log10(photon_density(index1:index2)) ;
P = polyfit(power_for_Fit, photon_for_Fit, 1);
    yfit = P(1)*power_for_Fit+P(2);
slope = P(1);

end

