function [ Consts ] = Define_constants( Pin_muW, nonrad_nonlin )
%DEFINE_CONSTANTS This function defines values of various constants used in
%solving the 
hbar = 6.626e-34 / (2*pi);
clight = 299792458;

%% properties of cavity
beta = 0.0005;
Gamma = 0.1 *1e-2;
tauC = 100*1e-9;

%% properties of 2D material
Bnr_by_Br = nonrad_nonlin * 100;
Cbx = nonrad_nonlin * 0.35; % cm2/s
a_gain = 1e-6; %cm2
tauR = 100*1e-12;
flake_area = 15e-6*pi/6*0.5e-6 *1e4; % R *theta * mode size : unit cm^2

%% properties of pump
lambda_p_nm = 670;
wp = 2*pi*clight / (lambda_p_nm*1e-9);
Rpump = Pin_muW*1e-6/(flake_area*hbar*wp);

%% Create struct of constants
Consts = struct('Rpump', Rpump, 'tauR', tauR, 'tauC', tauC, 'Cbx', Cbx, ...
    'Bnr_by_Br', Bnr_by_Br, 'a_gain', a_gain, 'beta', beta, 'Gamma', Gamma);

end

