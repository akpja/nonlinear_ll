function [ F ] = LL_function( p, Consts )
%LL_FUNCTION Summary of this function goes here


n = calc_N_from_P( p, Consts );

F = Consts.Rpump - ((n/Consts.tauR + Consts.Cbx * n^2) ...
    + Consts.a_gain*n*p*Consts.beta/Consts.tauR)*(1+Consts.Bnr_by_Br);

end

