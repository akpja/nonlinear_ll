function [p , n, Rpump, fitfun  ] = nonlinear_LL_2D(Pin_muW, nonrad_nonlin)

%% define constants

if nonrad_nonlin ~= 1 && nonrad_nonlin ~= 0
    error('nonrad_nonlin can only be 1 or 0');
end

Consts = Define_constants(Pin_muW, nonrad_nonlin);
Rpump = Consts.Rpump;

%% solve using random starting points
rng default % for reproducibility
Pr = 500; % try 500 random start points
LA = log10(1e-30); LB = log10(1e100);
pts = 10.^(LA + (LB-LA) * rand(Pr, 1));
soln = zeros(Pr,1); % allocate solution
res = inf*ones(Pr, 1);
%options = optimset('PlotFcns',{@optimplotx,@optimplotfval});
%opts = optimoptions('fsolve','Display','off');
for k = 1:Pr
    [soln(k), res(k)] = fminbnd(@(p) abs(LL_function(p, Consts)), 0, pts(k,:)); % find solutions
 %   [soln(k), res(k)] = fzero(@(p) LL_function(p, Consts), [0 pts(k,:)]); % find solutions

end

soln_index = find(abs(res) == min(abs(res)),1);
fitfun = abs(res(soln_index));
p = soln(soln_index);
n = calc_N_from_P( p, Consts );


%% define local functions







end










