function [ ] = Emission_rates( N, P, Pin_muW, nonrad_nonlin )
%EMISSION_RATES Summary of this function goes here
%   Detailed explanation goes here

Consts = Define_constants(Pin_muW, nonrad_nonlin);
Rate_cavity_sp = Consts.beta*N/Consts.tauR;
Rate_st = Consts.beta * Consts.a_gain * N.* P /Consts.tauR;
Rate_nr = (1+Consts.Bnr_by_Br)*Consts.Cbx*N.^2 + Consts.Bnr_by_Br*N/Consts.tauR;

figure;
loglog(Pin_muW, Rate_cavity_sp, 'r-', 'linewidth', 2); hold on;
loglog(Pin_muW, Rate_nr, 'g-', 'linewidth', 2);
loglog(Pin_muW, Rate_st, 'b-', 'linewidth', 2);
legend('Cavity spontaneous', 'Total nonradiative', 'Stimulated', 'Location', 'NorthWest');
title(maketitle( nonrad_nonlin ));
ylabel('Emission rate (s^{-1})')
xlabel('Pump power (\mu W)')
end

