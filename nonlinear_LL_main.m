clear;
nonrad_nonlin = 0; % set to 0 if don't want to consider Auger and defects
plotzero = 1; % set to 0 if don't want to plot the dN/dt


%% solve the LL equations
Pin_muW = logspace(-8, 10, 100);
photon_density = 0*Pin_muW;
exciton_density = 0 *Pin_muW;
Rpump = 0*Pin_muW;
zerofun = 0*Pin_muW;
for i = 1: length(Pin_muW)
   [photon_density(i) , exciton_density(i), Rpump(i), zerofun(i)]...
       = nonlinear_LL_2D(Pin_muW(i), nonrad_nonlin); 
end


%% select the range for lower and upper slopes
% lower slope
[power_for_Fit_Low, yfitLow, slopeLow] = fit_LL(photon_density, Pin_muW, 'below');
% upper slope
[power_for_Fit_High, yfitHigh, slopeHigh] = fit_LL(photon_density, Pin_muW, 'above');

%% plot N and P versus Pin(muW)
% plot P
figure;
loglog(Pin_muW, photon_density, 'r-', 'linewidth', 2); hold on;
loglog(10.^(power_for_Fit_Low), 10.^(yfitLow), 'g--', 'linewidth', 3);
loglog(10.^(power_for_Fit_High), 10.^(yfitHigh), 'b--', 'linewidth', 3);
ylabel('Photon density (cm^{-2})')
xlabel('Pump power (\mu W)')
title(maketitle( nonrad_nonlin ));
legend('numerical LL curve', strcat('Fit slope (below P_{th}) = ', num2str(slopeLow, '%1.3f')), ...
    strcat('Fit slope (above P_{th}) = ', num2str(slopeHigh, '%1.3f')), 'Location', 'NorthWest');
% plot N
figure;
%yyaxis right
loglog(Pin_muW, exciton_density, 'r-', 'linewidth', 2)
ylabel('Exciton density (cm^{-2})')
xlabel('Pump power (\mu W)')
title(maketitle( nonrad_nonlin ));


%% save the figure (uncomment all of the following lines)
% main_dir = cd();
% cd('./figures')
% saveas(gcf, strcat(titleStr, '.fig'));
% cd(main_dir)

%% plot emission rates
Emission_rates( exciton_density, photon_density, Pin_muW, nonrad_nonlin )


%% plot the zero function dN/dt (should ideally be zero)
if plotzero == 1
    figure;
    xlabel('Pump power (\mu W)')
    loglog(Pin_muW, zerofun./Rpump, 'bo-'); 
    ylabel('(dN / dt ) / (dNp / dt) ');
    title('Health of root finding');
end